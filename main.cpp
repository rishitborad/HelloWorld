
#include "tasks.hpp"
#include "examples/examples.hpp"
#include "command_handler.hpp"
#include "stdio.h"
#include "io.hpp"


CMD_HANDLER_FUNC(tempHandler){
    printf("Your room temperature is : %d", (int)TS.getFarenheit());
    return true;
}
int main(void)
{

    
    scheduler_add_task(new terminalTask(PRIORITY_HIGH));

    scheduler_start(); ///< This shouldn't return
    return -1;
}



/*************terminal.cpp**************/

//add below line below //CMD_HANDLER_FUNC(tempHandler);

cp.addHandler(tempHandler,  "GetTemp", "Get your room temperature"); 